
import './App.css';
import React, {useState, useEffect} from 'react';
// import React from 'react';
function App() {
  const [shoestore, setShoestore] = useState([]);
  const url = "http://127.0.0.1:8000/api/shoe/";
  useEffect(() => {
    fetch(url)
    .then(res => res.json())
    .then(data => setShoestore(data));
  }, []);
  return (
    <div>
    <h1>SHOE STORE</h1>
    
    {shoestore.map((p) => (
      <ul>
      <li>{p.manufacturer}</li>
      <li>{p.brand_name}</li>
      <li>{p.size}</li>
      <li>{p.color}</li>
      <li>{p.material}</li>
      <li>{p.shoe_type}</li>
      <li>{p.fasten_type}</li>
      </ul>
      
    ))}
    
    </div>
  );
}
// class ShoestoreApp extends React.component {
//   url = 'http://127.0.0.1:8000/api/shoe/';
//   constructor(props) {
//     super(props);
//     this.state = { shoestore: [] };
//   }
//   componentDidMount() {
//     fetch(this.url)
//     .then((res) => res.json())
//     .then(data => this.setState({ shoestore:data.results}));
//   }
//   render() {
//     return (
//       <div>
//         <h1>SHOE STORE</h1>
//         <ul>
//           {this.state.shoestore.map((p) => (
//             <li>{p.manufacturer}</li>
//           ))}
//           </ul>
//           </div>
//     );
  // }
// }
export default App;
